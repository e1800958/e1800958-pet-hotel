package fi.vamk.e1800958.pethotel.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fi.vamk.e1800958.pethotel.DTO.BookingDTO;
import fi.vamk.e1800958.pethotel.Entity.Booking;
import fi.vamk.e1800958.pethotel.Entity.User;
import fi.vamk.e1800958.pethotel.Repository.BookingRepository;
import fi.vamk.e1800958.pethotel.Repository.UserRepository;

@RestController
public class BookingController {
	
	@Autowired
	private BookingRepository bookingRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@GetMapping("/bookings")
	public Iterable<BookingDTO> list(){
		try {
			ArrayList<BookingDTO> list= new ArrayList<BookingDTO>();
			for (Booking b : bookingRepository.findAll()) {
				list.add(b.convert());
			}
			return list;
		} catch (Exception e) {
			return null;
		}
		
	}
	
	
	@GetMapping("/booking-user/{user_id}")
	public Iterable<BookingDTO> listByUser(@PathVariable("user_id") int user_id){
		try {
			ArrayList<BookingDTO> list= new ArrayList<BookingDTO>();
			User u =userRepository.findById(user_id).get();
			for (Booking b : bookingRepository.findByUser(u)) {
				list.add(b.convert());
			}
			return list;
		} catch (Exception e) {
			return null;
		}
		
	}
	
	@GetMapping("/booking/{id}")
	public BookingDTO getBookingById(@PathVariable("id") int id) {
		try {
			return bookingRepository.findById(id).get().convert();
		} catch (Exception e) {
			return null;
		}
		
	}
	
	@PostMapping("/booking")
	public @ResponseBody BookingDTO newBooking(@RequestBody BookingDTO bookingDTO) {
		User u= userRepository.findById(bookingDTO.getUser_id()).get();
		return bookingRepository.save(bookingDTO.convert(u)).convert();
	}
	
	@PutMapping("/booking")
	public @ResponseBody BookingDTO updateBooking(@RequestBody BookingDTO bookingDTO) {
		User u= userRepository.findById(bookingDTO.getUser_id()).get();
		return bookingRepository.save(bookingDTO.convert(u)).convert();
	}
	
	@DeleteMapping("/booking/{id}")
	public void delete(@PathVariable("id") int id) {
		try {
			bookingRepository.deleteById(id);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}

}
