package fi.vamk.e1800958.pethotel.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fi.vamk.e1800958.pethotel.DTO.UserDTO;
import fi.vamk.e1800958.pethotel.Entity.User;
import fi.vamk.e1800958.pethotel.Repository.UserRepository;

@RestController
public class UserController {
	@Autowired
	private UserRepository userRepository;
	
	@PostMapping("/user")
	public @ResponseBody UserDTO newUser(@RequestBody UserDTO userDTO) {
		return userRepository.save(userDTO.convert()).convert();
	}
	
	@PutMapping("/user")
	public @ResponseBody UserDTO updateUser(@RequestBody UserDTO userDTO) {
		return userRepository.save(userDTO.convert()).convert();
	}
	
	@GetMapping("/users")
	public Iterable<UserDTO> list(){
		ArrayList<UserDTO> list= new ArrayList<UserDTO>();
		for (User u : userRepository.findAll()) {
			list.add(u.convert());
		}
		return list;
	}
	
	@GetMapping("/user-id/{id}")
	public UserDTO getUserByID(@PathVariable("id") int id) {
		try {
			return userRepository.findById(id).get().convert();
		} catch (Exception e) {
			return null;
		}
//		return userRepository.findById(id).get().convert();
	}
	
	@DeleteMapping("/user/{id}")
	public void delete(@PathVariable("id") int id) {
		try {
			userRepository.deleteById(id);
		} catch (Exception e) {
			
		}
		
	}

}
