package fi.vamk.e1800958.pethotel.Entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import fi.vamk.e1800958.pethotel.DTO.UserDTO;

@Entity
@Table(name="User")
@NamedQuery(name = "User.findAll", query = "SELECT p FROM User p")
public class User implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String name;
	private String email;
	private String phone;
	private String status;
	
	@OneToMany(mappedBy = "user_id")
	private List<Booking> bookings = new ArrayList<Booking>();
	
	public User() {
		super();
	}
	
	public User(int id, String name, String email, String phone, String status) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.status = status;
	}
	
	
	public User(String name, String email, String phone, String status) {
		super();
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.status = status;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public List<Booking> getBookings() {
		return bookings;
	}
	public void setBookings(List<Booking> bookings) {
		this.bookings = bookings;
	}
	
	public UserDTO convert() {
		if(this == null) return null;
		return  new UserDTO(id, name, email, phone, status);
	}
	
	@Override
	public String toString() {
		return "UserEntity [id=" + id + ", name=" + name + ", email=" + email + ", phone=" + phone + ", status="
				+ status  + "]";
	}


}
