package fi.vamk.e1800958.pethotel.Entity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import fi.vamk.e1800958.pethotel.DTO.BookingDTO;

@Entity
@Table(name="Booking")
@NamedQuery(name = "Booking.findAll", query = "SELECT p FROM Booking p")
public class Booking implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String owner;
	private Date period_start;
	private Date period_end;
	private Date time;
	private String pet;
	private String status;
	private double fee;
	
	@ManyToOne
    @JoinColumn(name = "user")	
	private User user_id;
	
	

	public Booking() {
		super();
	}
	

	public Booking(String owner, Date period_start, Date period_end, Date time, String pet, String status, double fee,
			User user_id) {
		super();
		this.owner = owner;
		this.period_start = period_start;
		this.period_end = period_end;
		this.time = time;
		this.pet = pet;
		this.status = status;
		this.fee = fee;
		this.user_id = user_id;
	}



	public Booking(int id, String owner, Date period_start, Date period_end, Date time, String pet,
			String status, double fee, User user_id) {
		super();
		this.id = id;
		this.owner = owner;
		this.period_start = period_start;
		this.period_end = period_end;
		this.time = time;
		this.pet = pet;
		this.status = status;
		this.fee = fee;
		this.user_id = user_id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Date getPeriod_start() {
		return period_start;
	}

	public void setPeriod_start(Date period_start) {
		this.period_start = period_start;
	}

	public Date getPeriod_end() {
		return period_end;
	}

	public void setPeriod_end(Date period_end) {
		this.period_end = period_end;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getPet() {
		return pet;
	}

	public void setPet(String pet) {
		this.pet = pet;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public double getFee() {
		return fee;
	}

	public void setFee(double fee) {
		this.fee = fee;
	}

	public User getUser_id() {
		return user_id;
	}

	public void setUser_id(User user_id) {
		this.user_id = user_id;
	}
	
	public BookingDTO convert() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		BookingDTO bookingDTO= new BookingDTO();
		bookingDTO.setId(this.id);
		bookingDTO.setOwner(this.owner);
		bookingDTO.setPeriod_start(formatter.format(this.period_start));
		bookingDTO.setPeriod_end(formatter.format(this.period_start));
		bookingDTO.setTime(formatter.format(this.period_start));
		bookingDTO.setPet(this.pet);
		bookingDTO.setStatus(this.status);
		bookingDTO.setFee(this.fee);
		bookingDTO.setUser_id(this.user_id.getId());
		return bookingDTO;
	}
	
	

}
