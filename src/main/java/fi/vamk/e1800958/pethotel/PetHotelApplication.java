package fi.vamk.e1800958.pethotel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import fi.vamk.e1800958.pethotel.DTO.BookingDTO;
import fi.vamk.e1800958.pethotel.DTO.UserDTO;
import fi.vamk.e1800958.pethotel.Entity.User;
import fi.vamk.e1800958.pethotel.Repository.BookingRepository;
import fi.vamk.e1800958.pethotel.Repository.UserRepository;

@SpringBootApplication
public class PetHotelApplication {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BookingRepository bookingRepository;

	public static void main(String[] args) {
		SpringApplication.run(PetHotelApplication.class, args);
	}
	
	@Bean
	public void init() {
		UserDTO u1= new UserDTO( "Huy Nguyen", "abc", "123", "enabled");
		UserDTO u2= new UserDTO( "Hoang Nguyen", "qwe", "678", "enabled");
		User saved1= userRepository.save(u1.convert());
		User saved2= userRepository.save(u2.convert());
		BookingDTO b1= new BookingDTO( "Huy Nguyen", "04-09-2020", "07-04-2020", "04-09-2020", "Dog", "Finished", 50, saved1.getId());
		BookingDTO b2= new BookingDTO( "Huy Nguyen", "03-09-2020", "08-04-2020", "03-09-2020", "Dog", "On Going", 50, saved1.getId());
		bookingRepository.save(b1.convert(saved1));
		bookingRepository.save(b2.convert(saved1));
		
		BookingDTO b3= new BookingDTO( "Hoang Nguyen", "03-09-2020", "08-04-2020", "03-09-2020", "Dog", "On Going", 50, saved2.getId());
		bookingRepository.save(b3.convert(saved2));
	}

}
