package fi.vamk.e1800958.pethotel.DTO;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import fi.vamk.e1800958.pethotel.Entity.Booking;
import fi.vamk.e1800958.pethotel.Entity.User;
import fi.vamk.e1800958.pethotel.Repository.BookingRepository;
import fi.vamk.e1800958.pethotel.Repository.UserRepository;


public class BookingDTO {
	private int id;
	private String owner;
	private String period_start;
	private String period_end;
	private String time;
	private String pet;
	private String status;
	private double fee;
	private int user_id;
	

	
	public BookingDTO(int id, String owner, String period_start, String period_end, String time, String pet,
			String status, double fee, int user_id) {
		super();
		this.id = id;
		this.owner = owner;
		this.period_start = period_start;
		this.period_end = period_end;
		this.time = time;
		this.pet = pet;
		this.status = status;
		this.fee = fee;
		this.user_id = user_id;
	}
	
	
	public BookingDTO(String owner, String period_start, String period_end, String time, String pet, String status,
			double fee, int user_id) {
		super();
		this.owner = owner;
		this.period_start = period_start;
		this.period_end = period_end;
		this.time = time;
		this.pet = pet;
		this.status = status;
		this.fee = fee;
		this.user_id = user_id;
	}


	public BookingDTO() {
		super();
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getOwner() {
		return owner;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getPeriod_start() {
		return period_start;
	}
	public void setPeriod_start(String period_start) {
		this.period_start = period_start;
	}
	public String getPeriod_end() {
		return period_end;
	}
	public void setPeriod_end(String period_end) {
		this.period_end = period_end;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getPet() {
		return pet;
	}
	public void setPet(String pet) {
		this.pet = pet;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public double getFee() {
		return fee;
	}
	public void setFee(double fee) {
		this.fee = fee;
	}

	public Booking convert(User user) {
		
		try {
			Booking bookingEntity= new Booking();
			bookingEntity.setId(this.id);
			bookingEntity.setOwner(this.owner);
			bookingEntity.setPeriod_start(new SimpleDateFormat("dd-MM-yyyy").parse(this.period_start));
			bookingEntity.setPeriod_end(new SimpleDateFormat("dd-MM-yyyy").parse(this.period_end));
			bookingEntity.setTime(new SimpleDateFormat("dd-MM-yyyy").parse(this.time));
			bookingEntity.setPet(this.pet);
			bookingEntity.setStatus(this.status);
			bookingEntity.setFee(this.fee);
			bookingEntity.setUser_id(user);
			return bookingEntity;
		} catch (ParseException e) {
			System.out.println(e);
			return null;
		}
		
	}
	
	@Override
	public String toString() {
		return "BookingDTO [id=" + id + ", owner=" + owner + ", period_start=" + period_start + ", period_end="
				+ period_end + ", time=" + time + ", pet=" + pet + ", status=" + status + ", fee=" + fee + "]";
	}
	
	

}
