package fi.vamk.e1800958.pethotel.DTO;

import fi.vamk.e1800958.pethotel.Entity.User;

public class UserDTO {
	private int id;
	private String name;
	private String email;
	private String phone;
	private String status;
	public UserDTO(int id, String name, String email, String phone, String status) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.status = status;
	}
	
	public UserDTO(String name, String email, String phone, String status) {
		super();
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.status = status;
	}
	
	public UserDTO() {
		super();
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public User convert() {
		
		User userEntity= new User();
		userEntity.setId(this.id);
		userEntity.setName(this.name);
		userEntity.setEmail(this.email);
		userEntity.setPhone(this.phone);
		userEntity.setStatus(this.status);
		return userEntity;
		
	}
	
	@Override
	public String toString() {
		return "UserDTO [id=" + id + ", name=" + name + ", email=" + email + ", phone=" + phone + ", status=" + status
				+ "]";
	}
	

}
