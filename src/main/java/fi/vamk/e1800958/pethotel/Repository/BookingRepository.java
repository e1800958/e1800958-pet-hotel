package fi.vamk.e1800958.pethotel.Repository;


import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import fi.vamk.e1800958.pethotel.Entity.Booking;
import fi.vamk.e1800958.pethotel.Entity.User;

public interface BookingRepository extends CrudRepository<Booking, Integer>{
	
	@Query(value="SELECT b FROM Booking b WHERE b.user_id = ?1")
	public List<Booking> findByUser( User user);
	

}
