package fi.vamk.e1800958.pethotel.Repository;

import org.springframework.data.repository.CrudRepository;


import fi.vamk.e1800958.pethotel.Entity.User;

public interface UserRepository extends CrudRepository<User, Integer>{
	
}
