package fi.vamk.e1800958.pethotel;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.apache.commons.collections4.IterableUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import fi.vamk.e1800958.pethotel.DTO.BookingDTO;
import fi.vamk.e1800958.pethotel.DTO.UserDTO;
import fi.vamk.e1800958.pethotel.Entity.Booking;
import fi.vamk.e1800958.pethotel.Entity.User;
import fi.vamk.e1800958.pethotel.Repository.BookingRepository;
import fi.vamk.e1800958.pethotel.Repository.UserRepository;

@SpringBootTest
@AutoConfigureMockMvc

class PetHotelApplicationTests extends AbstractTest {
	
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private BookingRepository bookingRepository;
	@Autowired
    private MockMvc mvc;


	@Test
	//Testcase 1, the application should save, fetch by id and delete user the user
    public void testCase1() {
		Iterable<User> begin= userRepository.findAll();
		User u1= new User("abc", "qwe", "1267", "enabled");
		User saved= userRepository.save(u1);
		User found= userRepository.findById(u1.getId()).get();
		// Test fetch user
		assertThat(found.toString()).isEqualTo(u1.toString());
		userRepository.delete(found);
		Iterable<User> end= userRepository.findAll();
		//test delete the user
		assertEquals((long) IterableUtils.size(begin), (long) IterableUtils.size(end));
	}
	
	@Test
	//Testcase 2, the application should save, fetch by id, by user_id, delete the booking
    public void postGetDeleteAtteandance() {
		Iterable<User> begin= userRepository.findAll();
		
		User u1= new User("abc", "qwe", "1267", "enabled");
		
		User saved= userRepository.save(u1);
		BookingDTO booking= new BookingDTO("abc", "03-03-2020", "03-04-2020", "02-03-2020", "Cat", "In progress", 40, u1.getId());

		//Save the booking
		Booking bookingsave=bookingRepository.save(booking.convert(saved));
		//System.out.println(bookingsave.convert().toString());
		BookingDTO foundById= bookingRepository.findById(bookingsave.getId()).get().convert();
		//System.out.println(foundById.toString());
		BookingDTO foundByUser= bookingRepository.findByUser(saved).get(0).convert();
		//System.out.println(foundByUser.toString());
		assertThat(foundById.toString()).isEqualTo(bookingsave.convert().toString().toString());
		assertThat(foundByUser.toString()).isEqualTo(bookingsave.convert().toString().toString());
	}
	
	@WithMockUser("USER")
	@Test
	// Testcase 3, test get list methods
	public void getList() throws  Exception {
		//Test get list of booking
		String uri_bookings = "/bookings";
	   MvcResult mvcResult_booking = mvc.perform(MockMvcRequestBuilders.get(uri_bookings)
	      .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
	   
	   int status_bookings = mvcResult_booking.getResponse().getStatus();
	   assertEquals(200, status_bookings);
	   String content_bookings = mvcResult_booking.getResponse().getContentAsString();
	   BookingDTO [] bookinglist= super.mapFromJson(content_bookings, BookingDTO [].class);
	   assertTrue(bookinglist.length > 0);
	   
	   
	 //Test get list of users
	   String uri_users = "/users";
	   MvcResult mvcResult_users = mvc.perform(MockMvcRequestBuilders.get(uri_users)
	      .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
	   
	   int status_users = mvcResult_users.getResponse().getStatus();
	   assertEquals(200, status_users);
	   String content_users = mvcResult_users.getResponse().getContentAsString();
	   UserDTO [] userlist= super.mapFromJson(content_users, UserDTO [].class);
	   assertTrue(userlist.length > 0);
	}
	
	
	@WithMockUser("USER")
	@Test
	// Testcase4, test get method by id, the respone should be 200 
	public void testGetById() throws  Exception {
		Random rand = new Random();
		int id= rand.nextInt(1000);
		//Test get booking by id
		String uri_bookings = "/booking/" + id;
	    MvcResult mvcResult_booking = mvc.perform(MockMvcRequestBuilders.get(uri_bookings)
	      .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
	   
	    int status_bookings = mvcResult_booking.getResponse().getStatus();
	    assertEquals(200, status_bookings);
	   
	   
	    //Test get user by id
	    String uri_users = "/user-id/" + id;
	    MvcResult mvcResult_users = mvc.perform(MockMvcRequestBuilders.get(uri_users)
	      .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
	   
	    int status_users = mvcResult_users.getResponse().getStatus();
	    assertEquals(200, status_users);
	}
	
	
	@WithMockUser("USER")
	@Test
	// Testcase5, test get booking method by user, the respone should be 200 
	public void testGetBookingByUser() throws  Exception {
		Random rand = new Random();
		int id= rand.nextInt(1000);
		//Test get booking by id
		String uri_bookings = "/booking-user/" + id;
	    MvcResult mvcResult_booking = mvc.perform(MockMvcRequestBuilders.get(uri_bookings)
	      .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
	   
	    int status_bookings = mvcResult_booking.getResponse().getStatus();
	    assertEquals(200, status_bookings);
	}
	
	
	@WithMockUser("USER")
	@Test
	// Testcase6, test post method, the respone should be 200 
	public void testPost() throws  Exception {
		//Test post booking
		String uri_bookings = "/booking";
		BookingDTO b1= new BookingDTO( "Huy Nguyen", "04-09-2020", "07-04-2020", "04-09-2020", "Dog", "Finished", 50, 1);
		String inputJson_booking = super.mapToJson(b1);
		MvcResult mvcResult_booking = mvc.perform(MockMvcRequestBuilders.post(uri_bookings)
			      .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson_booking)).andReturn();
		int status_booking = mvcResult_booking.getResponse().getStatus();
		assertEquals(200, status_booking);
		
		//Test post user
		UserDTO u1= new UserDTO( "Huy Nguyennbvvnv", "abc", "123", "enabled");
		String uri_user = "/user";
		String inputJson_user = super.mapToJson(u1);
		MvcResult mvcResult_user = mvc.perform(MockMvcRequestBuilders.post(uri_user)
			      .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson_user)).andReturn();
		int status_user = mvcResult_user.getResponse().getStatus();
		assertEquals(200, status_user);

	}
	
	
	@WithMockUser("USER")
	@Test
	// Testcase7, test put booking method, the respone should be 200 
	public void testPut() throws  Exception {
		//Test post booking
		String uri_bookings = "/booking";
		BookingDTO b1= new BookingDTO(1, "Huy Nguyen", "04-09-2020", "07-04-2020", "04-09-2020", "Dog", "Finished", 50, 1);
		String inputJson_booking = super.mapToJson(b1);
		MvcResult mvcResult_booking = mvc.perform(MockMvcRequestBuilders.put(uri_bookings)
			      .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson_booking)).andReturn();
		int status_booking = mvcResult_booking.getResponse().getStatus();
		assertEquals(200, status_booking);
		
		//Test post user
		UserDTO u1= new UserDTO( 1,"Huy Nguyennbvvnv", "abc", "123", "enabled");
		String uri_user = "/user";
		String inputJson_user = super.mapToJson(u1);
		MvcResult mvcResult_user = mvc.perform(MockMvcRequestBuilders.put(uri_user)
			      .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson_user)).andReturn();
		int status_user = mvcResult_user.getResponse().getStatus();
		assertEquals(200, status_user);

	}
	
	
	@WithMockUser("USER")
	@Test
	// Testcase8, test put booking method, the respone should be 200 
	public void testDelete() throws  Exception {
		//Test post booking
		String uri_bookings = "/booking/1";
		MvcResult mvcResult_booking = mvc.perform(MockMvcRequestBuilders.delete(uri_bookings)
			      .contentType(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		int status_booking = mvcResult_booking.getResponse().getStatus();
		assertEquals(200, status_booking);
		
		//Test post user
		String uri_user = "/user/1";
		MvcResult mvcResult_user = mvc.perform(MockMvcRequestBuilders.delete(uri_user)
			      .contentType(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		int status_user = mvcResult_user.getResponse().getStatus();
		assertEquals(200, status_user);

	}	

}
